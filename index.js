function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		target.health = target.health - this.attack
	};

	this.faint = function() {
		if (this.health < 5){
			console.warn(this.name + " fainted.")
		}
		
	}
}

let blastoise = new Pokemon("Blastoise", 30);
console.log(blastoise);

let typhlosion = new Pokemon("Typhlosion", 25);
console.log(typhlosion);

typhlosion.tackle(blastoise)
blastoise.tackle(typhlosion)
typhlosion.tackle(blastoise)
blastoise.tackle(typhlosion)
typhlosion.tackle(blastoise)
typhlosion.tackle(blastoise)




console.log("Blastoise's Health: " + blastoise.health) 
console.log("Typhlosion's Health: " +typhlosion.health) 

blastoise.faint()
typhlosion.faint()